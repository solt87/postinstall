#!/bin/sh
## Create apt list file for Virtualbox

cat > /etc/apt/sources.list.d/virtualbox.list << EOL
## Virtualbox repo
deb http://download.virtualbox.org/virtualbox/debian jessie contrib
EOL

wget -v -O - https://www.virtualbox.org/download/oracle_vbox.asc | apt-key add -

exit 0

