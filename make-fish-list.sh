#!/bin/sh
## Make a repo list for the Friendly Interactive Shell

cat > /etc/apt/sources.list.d/fish.list << EOL
## Repository for the Friendly Interactive Shell
deb http://download.opensuse.org/repositories/shells:/fish:/release:/2/Debian_8.0/ /
deb-src http://download.opensuse.org/repositories/shells:/fish:/release:/2/Debian_8.0/ /
EOL

wget -O - http://download.opensuse.org/repositories/shells:fish:release:2/Debian_8.0/Release.key | apt-key add -

exit 0
