#!/bin/sh
## Install dependencies for Steam

## Enable 32-bit packages
dpkg --add-architecture i386

## Install the needed packages
apt-get update
apt-get install libgl1-nvidia-glx:i386  libgl1-mesa-dri:i386 \
                libgl1-mesa-glx:i386 libc6:i386


exit 0
