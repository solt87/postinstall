#!/bin/sh
## Install dependencies of Breach and Clear

## Update the system (if applicable)
apt-get update && apt-get upgrade

## Install the needed packages
apt-get install libglu1-mesa:i386 libxcursor1:i386

exit 0
