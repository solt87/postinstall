#!/bin/sh
## Create a list file for the CRAN repository

cat > /etc/apt/sources.list.d/r-cran.list << EOLF
## CRAN repositories for R packages:
deb http://cran.rapporter.net/bin/linux/debian jessie-cran3/
deb-src http://cran.rapporter.net/bin/linux/debian jessie-cran3/
EOLF

apt-key adv --keyserver keys.gnupg.net --recv-key 381BA480

exit 0

