#!/bin/sh
## Install and configure the proprietary nvidia driver.
## See https://wiki.debian.org/NvidiaGraphicsDrivers

# Update the whole system (if available):
apt-get update && apt-get upgrade

# Download and install the driver:
aptitude -r install linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,') nvidia-kernel-dkms

# Create configuration file:
mkdir /etc/X11/xorg.conf.d
/bin/echo -e 'Section "Device"\n\tIdentifier "My GPU"\n\tDriver "nvidia"\nEndSection' > /etc/X11/xorg.conf.d/20-nvidia.conf

exit 0
