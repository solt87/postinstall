#!/bin/sh
## Make apt list file for VideoLAN repo

cat > /etc/apt/sources.list.d/videolan.list << EOL
## Videolan repo
deb http://download.videolan.org/pub/debian/stable/ /
deb-src http://download.videolan.org/pub/debian/stable/ /
EOL

wget -O - http://download.videolan.org/pub/debian/videolan-apt.asc | apt-key add -

exit 0

