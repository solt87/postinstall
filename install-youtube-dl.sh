#!/bin/bash
## Install the latest youtube-dl from its site.

wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
chmod a+rx /usr/local/bin/youtube-dl

exit 0

