Postinstall to-do
=================

Introduction
------------

This is a list of steps to set up a newly installed GNU/Linux environment the
way I prefer it.

In this version I describe the steps as they have to be performed on a
Linux Lite 3.2 (based on Ubuntu 16.04, based on Debian) system.

To-Do
-----

1. Configure Grub:
    - Edit _/etc/default/grub_, and
      from the _CMDLINE..._ part remove _quiet_ and replace _splash_ with _nosplash_.

      If necessary, use _nomodeset_ and/or _vga=..._ .

2. Remove any unneeded packages:
    - Virtualbox and Qemu guest packages

3. Install available updates:

        $ sudo apt-get update
        $ sudo apt-get dist-upgrade

4. Install available (and needed) drivers.

5. Install needed tools:
    - [transfer staged package](https://gitlab.com/snippets/20857)
    - _build-essential_
    - [git](https://git-scm.com)